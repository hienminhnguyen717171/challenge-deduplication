package main

import (
	_ "embed"
	"encoding/json"
	"errors"
	"log"
	"time"
	nats "github.com/nats-io/nats.go"
)

//go:embed events.json
var data []byte
var nc *nats.Conn

func main() {
	streamEventsNATS()
}

func streamEventsNATS() {
	// Connect to the NATS server
	nc, err = nats.Connect("nats://nats:4222")
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
	defer nc.Close()
	// Subscribe to a subject
	nats_subject := "event_json_stream"
	subscribeEvents(nats_subject)
	events := filterEvents(0)
	publishEvents(events, nc, nats_subject)

}
func publishEvents(events []Event, nc *nats.Conn, subject string) {
	for _, message := range events {
		payload, err := json.Marshal(message)
		if err != nil {
			log.Println("Error encoding JSON:", err)
			continue
		}

		err = nc.Publish(subject, payload)
		if err != nil {
			log.Println("Error publishing message:", err)
		}

		time.Sleep(5*time.Millisecond)
	}
}

func subscribeEvents(subject string) {
	var deduplicateService DeduplicateEvent
	
	_, err = nc.Subscribe(subject, func(msg *nats.Msg) {
		var message Event
		err := json.Unmarshal(msg.Data, &message)
		if err != nil {
			log.Println("Error decoding JSON:", err)
			return
		}

		log.Printf("Received message: %+v\n", message)

		event, err := deduplicateService.Deduplicate(&message)
		if errors.Is(err, DuplicateEventError) {
			log.Println("duplicate found", message)
		} else if err == nil {
			ProcessEvent(event)
		}
	})
	
	if err != nil {
		log.Fatal(err)
	}
}

func filterEvents(afterBlockNumber int) []Event {
	events := make([]Event, 0)
	_ = json.Unmarshal(data, &events)

	filteredEvents := make([]Event, 0)

	for i := range events {
		if events[i].Block >= afterBlockNumber {
			filteredEvents = append(filteredEvents, events[i])
		}
	}

	return filteredEvents
}

