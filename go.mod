module events-dedup

go 1.19

require github.com/wangjia184/sortedset v0.0.0-20220209072355-af6d6d227aa7

require (
	github.com/alovn/go-bloomfilter v1.1.0 // indirect
	github.com/avast/retry-go v3.0.0+incompatible // indirect
	github.com/bits-and-blooms/bitset v1.11.0 // indirect
	github.com/bits-and-blooms/bloom/v3 v3.6.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-redis/redis/v7 v7.4.1 // indirect
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/lichao-mobanche/bloom-filter-redis v0.0.3 // indirect
	github.com/nats-io/nats.go v1.31.0 // indirect
	github.com/nats-io/nkeys v0.4.5 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/redis/go-redis/v9 v9.3.0 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
)
