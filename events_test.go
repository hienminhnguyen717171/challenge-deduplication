package main

import (
	"testing"
)

func TestProcessEvent(t *testing.T) {
	tests := []struct {
		name  string
		event *Event
	}{
		{
			name: "Test case 1",
			event: &Event{
				Hash:      "hash1",
				Block:     1,
				Timestamp: 123456789,
				Amount:    "10.5",
				From:      "sender1",
			},
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ProcessEvent(tt.event)
			// Add assertions if needed
			
		})
	}
}