# Challenge deduplication
- I stream events using NATS and deduplicate event using Bloom filter, persist in Redis 

# Set up 
- Go, NATS, Redis bloom filter 

# How to run 
- go mod tidy
- go run . 
- Run unit test : go test -v

- Run docker docker: - docker-compose up --build

# Other improvements 
- Built int NATS for deduplication: https://dev.to/claudiunicolaa/effortless-nats-message-deduplication-in-go-2ohl
- Stream events with multiple workers 
- Add more unit test 
