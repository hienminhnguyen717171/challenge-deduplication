# go source files, ignore vendor directory
SRC = $(shell find . -type f -name '*.go' -not -path "./vendor/*")

build:
	docker compose up --build

up:
	docker compose up

down:
	docker compose down

restart:
	docker compose restart

rebuild:
	docker compose up --build

fmt:
	@gofmt -l -w $(SRC)

run:
	go run main.go

test:
	go test -v /test
	go test -race -cover ./...

tools:
	go get golang.org/x/tools/cmd/goimports
	go get github.com/kisielk/errcheck
	go get github.com/golang/lint/golint
	go get github.com/axw/gocov/gocov
	go get github.com/matm/gocov-html
	go get github.com/tools/godep
	go get github.com/mitchellh/gox

tidy: FORCE
	go mod tidy

.DEFAULT_GOAL := build

fmt:
        go fmt ./...
.PHONY:fmt

lint: fmt
        golint ./...
.PHONY:lint

vet: fmt
        go vet ./...
.PHONY:vet

build: vet
        go build hello.go
.PHONY:build