package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/avast/retry-go"
	"github.com/go-redis/redis/v7"
	"github.com/lichao-mobanche/bloom-filter-redis/bloom"
)

var DuplicateEventError = errors.New("duplicate event")
var bf *bloom.BloomFilter
var rdb *redis.Client
var err error

func init() {

	rdb = redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	// Create a Bloom filter with a certain number of elements and false positive rate
	bf, err = bloom.NewBloomFilter(bloom.Redis, rdb, "bloomtestkey", 100000, 0.01, 10000)
	if err != nil {
		fmt.Print(err)
		
	}
}

// DeduplicateI is the interface for deduplicate service(can extend to deduplicate other datatype)
type DeduplicateI interface {
	Deduplicate(event *Event) int
}

// DeduplicateEvent is an implementation of the DeduplicateI interface
type DeduplicateEvent struct{}

// Deduplicate - checks if the event was processed before
// deduplicateEvent removes duplicate events based on the Bloom filter
func (ds DeduplicateEvent) Deduplicate(event *Event) (*Event, error) {

	if event == nil {
		log.Fatal("nil event provided", event)
		return nil, errors.New("nil event provided")
	}

	// Convert the event to JSON to create a unique identifier
	eventJSON, err := json.Marshal(event)
	if err != nil {
		log.Fatal("json.Marshal err", err)
		return nil, err
	}

	exist, _ := bf.Exists([]byte(eventJSON))
	if exist {
		return nil, DuplicateEventError
	}

	err = retry.Do(
		func() error {
			err = bf.Append([]byte(eventJSON))
			if err != nil {
				log.Fatal("bf.Append err", err)	
				return err 
			}
			return nil
		},
		retry.Attempts(uint(3)),
		retry.Delay(1*time.Second),
	)

	if err != nil {
		log.Fatal("addEvent err", err)
		return event, err
	}

	return event, nil
}
