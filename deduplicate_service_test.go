package main

import (
	"testing"
)

func TestDeduplicateEvent(t *testing.T) {
	// Create an instance of DeduplicateEvent
	deduplicator := DeduplicateEvent{}

	// Create a sample event
	event := &Event{
		Hash:      "abc123",
		Block:     123,
		Timestamp: 1632963200,
		Amount:    "10.50",
		From:      "sender123",
	}

	// Perform the deduplication
	deduplicatedEvent, err := deduplicator.Deduplicate(event)

	// Check for errors
	if err != nil {
		t.Fatalf("Deduplication failed with error: %v", err)
	}

	// Check if the deduplicated event is not nil
	if deduplicatedEvent == nil {
		t.Fatal("Deduplicated event is nil")
	}

	// Perform deduplication again with the same event
	duplicateEvent, err := deduplicator.Deduplicate(event)

	// Check if the expected DuplicateEventError is returned
	if err != DuplicateEventError {
		t.Fatalf("Expected DuplicateEventError, but got: %v", err)
	}

	// Check if the deduplicated event is nil for a duplicate event
	if duplicateEvent != nil {
		t.Fatal("Deduplicated event is not nil for a duplicate event")
	}
}
